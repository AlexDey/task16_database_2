-- MySQL Workbench Forward Engineering
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema studentDB
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema studentDB
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `studentDB` DEFAULT CHARACTER SET utf8 ;
USE `studentDB` ;

-- -----------------------------------------------------
-- Table `studentDB`.`region`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `studentDB`.`region` (
  `region` VARCHAR(25) NOT NULL,
  `region code` INT NOT NULL,
  PRIMARY KEY (`region`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `studentDB`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `studentDB`.`city` (
  `city` VARCHAR(25) NOT NULL,
  `region_region` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`city`, `region_region`),
  INDEX `fk_city_region1_idx` (`region_region` ASC) VISIBLE,
  CONSTRAINT `fk_city_region1`
    FOREIGN KEY (`region_region`)
    REFERENCES `studentDB`.`region` (`region`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `studentDB`.`finished secondary school`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `studentDB`.`finished secondary school` (
  `name` VARCHAR(25) NOT NULL,
  `phone number` INT NOT NULL,
  `director's name` VARCHAR(45) NOT NULL,
  `city_city` VARCHAR(25) NOT NULL,
  `city_region_region` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`name`, `city_city`, `city_region_region`),
  INDEX `fk_finished secondary school_city1_idx` (`city_city` ASC, `city_region_region` ASC) VISIBLE,
  CONSTRAINT `fk_finished secondary school_city1`
    FOREIGN KEY (`city_city` , `city_region_region`)
    REFERENCES `studentDB`.`city` (`city` , `region_region`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `studentDB`.`group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `studentDB`.`group` (
  `name` VARCHAR(45) NOT NULL,
  `group number` INT NOT NULL,
  `admission year` DATE NOT NULL,
  PRIMARY KEY (`group number`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `studentDB`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `studentDB`.`student` (
  `id` INT NOT NULL,
  `second name` VARCHAR(45) NOT NULL,
  `first name` VARCHAR(45) NOT NULL,
  `middle name` VARCHAR(45) NOT NULL,
  `general rating` DOUBLE NOT NULL,
  `date of birth` DATE NOT NULL,
  `enter date` DATE NOT NULL,
  `id card number` INT NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `id card & admission year` VARCHAR(45) GENERATED ALWAYS AS (CONCAT(id card number, -, admission year)) VIRTUAL,
  `full name` VARCHAR(45) GENERATED ALWAYS AS (CONCAT(first name, middle name, second name)) VIRTUAL,
  `student's admission age` INT GENERATED ALWAYS AS () VIRTUAL,
  `city_city` VARCHAR(25) NOT NULL,
  `finished secondary school_name` VARCHAR(25) NOT NULL,
  `group_group number` INT NOT NULL,
  PRIMARY KEY (`id`, `city_city`, `finished secondary school_name`, `group_group number`),
  UNIQUE INDEX `id card number_UNIQUE` (`id card number` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  INDEX `fk_student_city_idx` (`city_city` ASC) VISIBLE,
  INDEX `fk_student_finished secondary school1_idx` (`finished secondary school_name` ASC) VISIBLE,
  INDEX `fk_student_group1_idx` (`group_group number` ASC) VISIBLE,
  CONSTRAINT `fk_student_city`
    FOREIGN KEY (`city_city`)
    REFERENCES `studentDB`.`city` (`city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_finished secondary school1`
    FOREIGN KEY (`finished secondary school_name`)
    REFERENCES `studentDB`.`finished secondary school` (`name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_group1`
    FOREIGN KEY (`group_group number`)
    REFERENCES `studentDB`.`group` (`group number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `studentDB`.`debt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `studentDB`.`debt` (
  `subject` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`subject`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `studentDB`.`student_has_debt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `studentDB`.`student_has_debt` (
  `student_id` INT NOT NULL,
  `student_city_city` VARCHAR(25) NOT NULL,
  `student_finished secondary school_name` VARCHAR(25) NOT NULL,
  `debt_subject` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`student_id`, `student_city_city`, `student_finished secondary school_name`, `debt_subject`),
  INDEX `fk_student_has_debt_debt1_idx` (`debt_subject` ASC),
  INDEX `fk_student_has_debt_student1_idx` (`student_id` ASC, `student_city_city` ASC, `student_finished secondary school_name` ASC),
  CONSTRAINT `fk_student_has_debt_student1`
    FOREIGN KEY (`student_id` , `student_city_city` , `student_finished secondary school_name`)
    REFERENCES `studentDB`.`student` (`id` , `city_city` , `finished secondary school_name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_debt_debt1`
    FOREIGN KEY (`debt_subject`)
    REFERENCES `studentDB`.`debt` (`subject`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
